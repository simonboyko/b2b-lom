# HTML preprocessor files/ templates

As a HTML preprocessor and a template engine I use Pug.

## Base

The folder contains base templates that are used on the most or all pages and can be apply for the most different projects.


## Components

The folder contains pug templates that are created for each project individually. Those components are project-specific generally.


## Layout

The folder contains main parts on the page and base page-layout.


## Pages

The folder contains pug-files that will be compile in HTML.