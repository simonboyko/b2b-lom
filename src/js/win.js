document.addEventListener('DOMContentLoaded', function () {
  'use strict';

  var i; //counter
  var items = document.querySelectorAll('.win__item');
  var current = 0;
  var total = 1;
  var step = 0;
  var counterCurrent = document.querySelector('.counter__current');
  var counterTotal = document.querySelector('.counter__total');
  var counterWrp = document.querySelector('.win__counter');

  var btnYes = document.querySelector('.win__button.-yes');
  var btnNo = document.querySelector('.win__button.-no');

  var sliderYouClass = '.win__slider.-you';
  var sliderRivalClass = '.win__slider.-rival';
  var handleYou = document.querySelector('.win__slider.-you .win__handle');
  var lineYou = document.querySelector('.win__slider.-you .win__line');
  var handleRival = document.querySelector('.win__slider.-rival .win__handle');
  var lineRival = document.querySelector('.win__slider.-rival .win__line');

  var result = document.querySelector('.win__result');

  btnYes.addEventListener('click', pressYes);
  btnNo.addEventListener('click', pressNo);

  function setInitialValues() {
    for (i=0;i<items.length;i++);
    total = i;
    step = Math.round(100 / (total) * 100) / 100;
    counterCurrent.innerHTML = current + 1;
    counterTotal.innerHTML = i;

    // Set initial position for the handles and write this position to a data-position attribute
    handleYou.style.left = '0%';
    handleYou.setAttribute('data-position', '0');
    lineYou.style.right = '100%';
    lineYou.setAttribute('data-position', '100');
    handleRival.style.left = '0%';
    handleRival.setAttribute('data-position', '0');
    lineRival.style.right = '100%';
    lineRival.setAttribute('data-position', '100');

    // return console.log('Initial values are set: \n' +
    //   '  Total: ' + total + '\n' +
    //   '  Current: ' + (current +1) + '\n' +
    //   '  Step: ' + step + '%.\n' +
    //   '  Handles are set to 0%.');
  }

  function resetActiveItem() {
    var itemActive = document.querySelector('.win__item.-active');

    if (items[current]) {
      itemActive.classList.remove('-active');
      items[current].classList.add('-active');

      // return console.log('The active item is reset');
    } else {
      itemActive.classList.remove('-active');
      btnYes.classList.add('-hidden');
      btnNo.classList.add('-hidden');
      counterWrp.classList.add('-hidden');

      // return console.log('No item is yet active');
    }
  }

  function showResult() {
    result.classList.add('-shown');

    // return console.log('The result is shown.');
  }

  function updateCounter() {
    current++;
    if (!(current === total)) counterCurrent.innerHTML = current + 1;

    // return console.log('Counter is not updated. Current value: ' + current);
  }

  function moveHandle(sliderClass) {
    var handle = document.querySelector(sliderClass + ' .win__handle');
    var line = document.querySelector(sliderClass + ' .win__line');
    var handlePosition = +handle.getAttribute('data-position');
    var linePosition = +line.getAttribute('data-position');

    handle.style.left = (handlePosition + step) + '%';
    handle.setAttribute('data-position', String(handlePosition + step));

    line.style.right = (linePosition - step) + '%';
    line.setAttribute('data-position', String(linePosition - step));

    // return console.log('Moved handle from ' + handlePosition + '% to ' + (handlePosition + step) + '%\n' +
    //   'Moved line from ' + linePosition + '% to ' + (linePosition - step) + '%')
  }

  function pressYes() {
    updateCounter();
    resetActiveItem();
    moveHandle(sliderYouClass);
    moveHandle(sliderRivalClass);

    if (current >= total) {
      showResult();
    }
  }

  function pressNo() {
    updateCounter();
    resetActiveItem();
    moveHandle(sliderRivalClass);

    if (current >= total) {
      showResult();
      // return console.log('Pressed No, but it\'s not more items in the list');
    }
  }

  setInitialValues();

});