// function isTouchDevice() {
//   return 'ontouchstart' in document.documentElement;
// }

function isTouch() {
  if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
    return true;
  }
}

function splitWord(word) {
  var split = '';

  for (var i = 0; i < word.length; i++) {
    split = split + '<span>' + word[i] + '</span>';
  }

  return split;
}

function setInitialPosition(splitWord) {
  var chars = splitWord.getElementsByTagName('span');
  var charOffset = 0;
  var originWord = splitWord.getAttribute('data-right-word');

  for (var i = 0; i < chars.length; i++) {
    var charWrp = chars[i];
    var char = charWrp.innerHTML;
    charOffset = charWrp.offsetLeft;

    charWrp.setAttribute('data-position', charOffset);
  }

  for (var i = 0; i < chars.length; i++) {
    var charWrp = chars[i];
    var char = charWrp.innerHTML;
    for (var j = 0; j < originWord.length; j++) {
      if (char === originWord[j]) {
        charWrp.setAttribute('data-origin-position', j);
        originWord = originWord.replace(char, '0');
        break;
      }
    }
  }
}

function setWordWrpSize(els) {
  for (var i = 0; i < els.length; i++) {
    var el = els[i];
    var elWidth = el.clientWidth;
    var elHeight = el.clientHeight;

    el.style.width = elWidth + 'px';
    el.style.height = elHeight + 'px';
  }
}

function orderLetter(wordWrp) {
  var words = wordWrp.querySelectorAll('.mixWord');

  for (i = 0; i < words.length; i++) {
    var chars = words[i].querySelectorAll('.mixWord > span');
    var stringWidth = 0;

    for (var j = 0; j < chars.length; j++) {
      var char = words[i].querySelector('.mixWord > span[data-origin-position="' + j + '"]');
      var charValue = char.innerHTML;
      var offset = -(char.getAttribute('data-position') - stringWidth);

      char.style.order = '';
      char.style.transform = 'translateX(' + offset + 'px)';
      char.setAttribute('data-offset', offset);

      stringWidth = stringWidth + char.offsetWidth;
      // console.log(j + ': ' + offset);
    }
  }

  setTimeout(function () {
    for (i = 0; i < words.length; i++) {
      var chars = words[i].querySelectorAll('.mixWord > span');

      for (var j = 0; j < chars.length; j++) {
        var char = words[i].querySelector('.mixWord > span[data-origin-position="' + j + '"]');
        char.style.order = j;

        char.style.transform = 'translateX(0)';
        char.style.transitionDuration = '0s';
      }
    }
  },500);
}

function mixLetter(wordWrp) {
  var words = wordWrp.querySelectorAll('.mixWord');

  for (i = 0; i < words.length; i++) {
    var chars = words[i].querySelectorAll('.mixWord > span');

    for (var j = 0; j < chars.length; j++) {
      var char = words[i].querySelector('.mixWord > span[data-origin-position="' + j + '"]');
      var offset = char.getAttribute('data-offset');

      char.style.transform = 'translateX('+offset+'px)';
      char.style.order = null;

    }
  }

  setTimeout(function () {
    for (i = 0; i < words.length; i++) {
      var chars = words[i].querySelectorAll('.mixWord > span');

      for (var j = 0; j < chars.length; j++) {
        var char = words[i].querySelector('.mixWord > span[data-origin-position="' + j + '"]');

        char.style.transform = 'translateX(0px)';
        char.style.transitionDuration = '0.5s';
        char.style.transitionTimingFunction = 'cubic-bezier(0.39, 0.575, 0.565, 1)';
      }
    }
  }, 10);
}

function changeTipText(el) {
  if (isTouch()) el.innerHTML = 'Нажми для перевода';
}

document.addEventListener('DOMContentLoaded', function words() {
  'use strict';



  var words = document.querySelectorAll('.mixWord');
  var tip = document.querySelector('.values__tip');;

  changeTipText(tip);

  for (var i = 0; i < words.length; i++) {
    var word = words[i];
    word.innerHTML = splitWord(word.innerHTML);
    setTimeout(setInitialPosition,200,word);
    setTimeout(setWordWrpSize, 1000, words);
  }

  // обработчик на наведение мышкой над заголовком и отведение мыши
  var titles = document.querySelectorAll('.values__title');
  for (i = 0; i < titles.length; i++) {
    titles[i].addEventListener('mouseenter', function () {
      orderLetter(this);
    });
    titles[i].addEventListener('mouseleave', function () {
      mixLetter(this);
    });
  }
});