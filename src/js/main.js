$(document).ready(function() {
  $('[data-popup]').magnificPopup({
    type:'inline',
    midClick: true
  });
});

document.addEventListener('DOMContentLoaded', function () {
  'use strict';

  jQuery(function($){
    $("[data-phone]").mask("+7 (999) 999-99-99");
  });
});