var gulp = require('gulp');
var browserSync = require('browser-sync');
var injector = require('bs-html-injector');
var reload = browserSync.reload;
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var pug = require('gulp-pug');
var plumber = require('gulp-plumber');
var postcss = require('gulp-postcss');
var lost = require('lost');


var minify = require('gulp-minify');
var concat = require('gulp-concat');
var clean = require('gulp-clean');
var runSequence = require('run-sequence');
var gulpCopy = require('gulp-copy');
var csso = require('gulp-csso');
var rename = require('gulp-rename');
var critical = require('critical');

var path = {
  build: {
    html: 'build/',
    js: 'build/js/',
    css: 'build/css/',
    images: 'build/images/',
    fonts: 'build/fonts/',
    maps: 'build/maps/'
  },
  src: {
    html: 'src/',
    pug: 'src/pug/pages/*.pug',
    js: './src/js/**/*.js',
    sass: 'src/sass/**/*.scss',
    css: 'src/css/',
    maps: 'maps/',
    files: 'src/files/**/*',
    images: 'src/images/**/*.*',
    fonts: 'src/fonts/**/*.*'
  },
  watch: {
    html: 'src/pug/**/*.pug',
    js: 'src/js/**/*.js',
    sass: 'src/sass/**/*.scss'
  },
  clean: './build'
};


gulp.task('browserSync', function () {
  browserSync.use(injector);
  browserSync({
    server: {baseDir: path.src.html},
    notify: true,
    open: false,
    ghostMode: false
  });
});

gulp.task('js-concat', function () {
  return gulp.src([
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/magnific-popup/dist/jquery.magnific-popup.min.js',
    'node_modules/jquery.maskedinput/src/jquery.maskedinput.js',
    // 'node_modules/cleave.js/dist/cleave.min.js',
    // 'node_modules/cleave.js/dist/addons/cleave-phone.ru.js',
    path.src.js,
    '!./src/js/all*'
  ])
    .pipe(plumber())
    .pipe(concat('all.js'))
    .pipe(minify({
      ext: {
        min: '.min.js'
      }
    }))
    .pipe(clean('./src/js/all.js'))
    .pipe(gulp.dest('./src/js/'));
});
gulp.task('js-clean', function () {
  return gulp.src('./src/js/all.js')
    .pipe(clean());
});

gulp.task('sass', function () {
  gulp.src(['node_modules/magnific-popup/src/css/main.scss', path.src.sass])
    .pipe(sourcemaps.init({loadMap: true}))
    .pipe(sass()).on('error', sass.logError)
    .pipe(postcss([
      lost()
      // autoprefixer()
    ]))
    .pipe(autoprefixer({
      browsers: ['last 10 versions'],
      cascade: false
    }))
    .pipe(concat('styles.css'))
    .pipe(sourcemaps.write(path.src.maps))
    .pipe(gulp.dest(path.src.css))
    .pipe(reload({stream: true}));
});
gulp.task('pug', function buildHTML() {
  return gulp.src(path.src.pug)
    .pipe(plumber())
    .pipe(pug({
      // pretty: true
    }))
    .pipe(gulp.dest(path.src.html))
    .pipe(reload({stream: true}));
});
gulp.task('js', function () {
  runSequence('js-concat', 'js-clean');
});

gulp.task('critical', function () {
  critical.generate({
    inline: true,
    base: './build/',
    src: 'index.html',
    css: './build/css/styles.css',
    // extract: true,
    dest: 'index.html',
    minify: true,
    width: 1920,
    height: 930
  });
});

gulp.task('clean', function () {
  return gulp.src('./build', {read: false})
    .pipe(clean());
});

gulp.task('cssmin', function () {
  return gulp.src('./src/css/styles.css')
    .pipe(csso())
    // .pipe(rename("styles.min.css"))
    .pipe(gulp.dest('./build/css'));
});

gulp.task('html-copy', function () {
  return gulp.src(['./src/*.html', './src/browserconfig.xml', './src/site.webmanifest'])
    .pipe(gulp.dest('./build/'));
});

gulp.task('files-copy', function () {
  return gulp.src(['./src/files/**/*', '!README.md'])
    .pipe(gulp.dest('./build/files'));
});

gulp.task('images-copy', function () {
  return gulp.src(['./src/images/**/*', '!README.md'])
    .pipe(gulp.dest('./build/images'));
});

gulp.task('fonts-copy', function () {
  return gulp.src(['./src/fonts/**/*', '!README.md'])
    .pipe(gulp.dest('./build/fonts'));
});

gulp.task('js-copy', function () {
  return gulp.src('./src/js/all.min.js')
    .pipe(gulp.dest('./build/js/'));
});

gulp.task('build', function () {
  runSequence('clean', 'sass', 'cssmin', 'pug', 'html-copy', 'files-copy', 'images-copy', 'fonts-copy', 'js', 'js-copy', 'critical');
});


gulp.task('watch', function () {
  gulp.watch(path.watch.sass, function () {
    setTimeout(function () {
      gulp.start('sass');
    }, 100);
  });
  gulp.watch(path.watch.html, function () {
    setTimeout(function () {
      gulp.start('pug');
    }, 100);
  });
  gulp.watch(['./src/js/**/*.js', '!./src/js/all*'], function () {
    setTimeout(function () {
      gulp.start('js');
    }, 100);
  });
});
gulp.task('default', ['browserSync', 'sass', 'js', 'watch', 'pug']);
